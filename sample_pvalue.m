% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

% two-sided p-value for the null hypothesis that a single observation x is from the same distribution as values
function p = sample_pvalue(x, values)
    p_left = mean(x <= values, 'all');
    p_right = mean(x >= values, 'all');
    p = 2 * min(p_left, p_right);
end