% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

function [hmp, pDTE, pDICM, pICM_size, ppos] = fixed_internalization()

T = readtable('cellcounts_human.csv');

NpICM = T.ICM_GFPp; % total number of GFP+ cells in ICM
NpTE = T.TE_GFPp; % total number of GFP+ cells in TE

NnICM = T.ICM_GFPn; % total number of GFP- cells in ICM
NnTE = T.TE_GFPn; % total number of GFP- cells in TE

Np = NpICM + NpTE; % total number of GFP+ cells
Nn = NnICM + NnTE; % total number of GFP- cells
NICM = NpICM + NnICM; % total number of cells in ICM
NTE = NpTE + NnTE; % total number of cells in TE
N = NICM + NTE; % total number of cells

pos = Np ./ N; % fraction of GFP+ cells
ICM_size = NICM ./ N; % fraction of cells in the ICM

Nembryos = numel(N); % number of embryos

Nruns = 1e4; % number of repetition runs
arrest = 0.065; % mean fraction of cells arresting
death = 0.044; % mean fraction of cells dying
Nin_min = [0 0 1 0 0 zeros(1,15)]; % minimum number of cells internalizing
Nin_max = [0 0 3 2 1 zeros(1,15)]; % maximum number of cells internalizing
symbal = 0; % asymmetric/symmetric internalization balance
compint = true; % compensate for too few or too many cells already internalized?

% parameters to screen
Nin3 = 1:4; % number of cells internalizing after generation 3
Nin4 = 0:4; % number of cells internalizing after generation 4
Nin5 = 0:1; % number of cells internalizing after generation 5
bias = 0.5:0.02:0.98; % balance between GFP+ and GFP- cells that internalize

% ICM-dominant cell types in TE and ICM
DICM = NaN(Nembryos,1);
DTE = NaN(Nembryos,1);
for i = 1:Nembryos
    if (NpICM(i) > NnICM(i)) % GFP+ cells dominant in the ICM
        DICM(i) = NpICM(i) / NICM(i);
        DTE(i)  = NpTE(i)  / NTE(i);
    else % GFP- cells dominant in the ICM
        DICM(i) = NnICM(i) / NICM(i);
        DTE(i)  = NnTE(i)  / NTE(i);
    end
end
    
close all
f = figure;
hold on
box on
xlabel('2-cell blastomere bias to ICM (%)')
ylabel('P-value of TE/ICM composition, 2/(1/P_3+1/P_4)')
ylim([0 1])
xticks(10*floor(min(10*bias)):10:10*ceil(max(10*bias)))
xlim(minmax(xticks))
xticklabels(arrayfun(@(x) [num2str(x) ':' num2str(100-x)], xticks(), 'UniformOutput', false))
set(gca, 'FontSize', 18, 'LineWidth', 1)

T = table();
T.bias = 100 * bias';

% screen for the best internalization parameters
N1 = numel(Nin3) + numel(Nin4) + numel(Nin5);
N2 = numel(bias);
pDTE = NaN(N1,N2);
pDICM = pDTE;
pICM_size = pDTE;
ppos = pDTE;
delete(findall(0,'type','figure','tag','TMWWaitbar'))
wb = waitbar(0, '0 %', 'Name', 'Progress');
tic
for i1 = 1:N1
    use_Nin_min = Nin_min;
    use_Nin_max = Nin_max;
    if i1 <= numel(Nin3)
        use_Nin_min(3) = Nin3(i1);
        use_Nin_max(3) = Nin3(i1);
        name = [num2str(use_Nin_min(3)) ' , ' num2str(use_Nin_min(4)) '-' num2str(use_Nin_max(4)) ' , ' num2str(use_Nin_min(5)) '-' num2str(use_Nin_max(5))];
    elseif i1 <= numel(Nin3) + numel(Nin4)
        use_Nin_min(4) = Nin4(i1-numel(Nin3));
        use_Nin_max(4) = Nin4(i1-numel(Nin3));
        name = [num2str(use_Nin_min(3)) '-' num2str(use_Nin_max(3)) ' , ' num2str(use_Nin_min(4)) ' , ' num2str(use_Nin_min(5)) '-' num2str(use_Nin_max(5))];
    else
        use_Nin_min(5) = Nin5(i1-numel(Nin3)-numel(Nin4));
        use_Nin_max(5) = Nin5(i1-numel(Nin3)-numel(Nin4));
        name = [num2str(use_Nin_min(3)) '-' num2str(use_Nin_max(3)) ' , ' num2str(use_Nin_min(4)) '-' num2str(use_Nin_max(4)) ' , ' num2str(use_Nin_min(5))];
    end

    parfor i2 = 1:N2
        % simulate blastocyst development
        [pos_mod, ~, ~, ~, ICM_size_mod, DICM_mod, DTE_mod] = stochastic_blastocyst(N, arrest, death, use_Nin_min, use_Nin_max, bias(i2), symbal, compint, Nruns, false);
        
        % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of ICM-dominant cells
        [~, pDTE(i1,i2)] = kstest2(DTE, DTE_mod(:));
        [~, pDICM(i1,i2)] = kstest2(DICM, DICM_mod(:));
        
        % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted fraction of cells that ended up in the ICM
        [~, pICM_size(i1,i2)] = kstest2(ICM_size, ICM_size_mod(:));
        
        % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of GFP+ cells in whole embryo
        [~, ppos(i1,i2)] = kstest2(pos, pos_mod(:));
    end

    hmp = 2 ./ (1./pDTE + 1./pDICM); % harmonic mean p-value

    figure(f);
    plot(100*bias, squeeze(hmp(i1,:)), 'LineWidth', 2, 'DisplayName', ['n_{inter} = ' name])
    
    T.(['hmp_ninter' strrep(strrep(name, ' , ', '_'), '-', '')]) = squeeze(hmp(i1,:))';

    progress = i1 / N1;
    waitbar(progress, wb, sprintf('%i %% (time left: %s)', floor(100 * progress), duration(0,0,toc*(1-progress)/progress)));
end
toc
close(wb)

writetable(T, 'ninter_bias.csv');

end