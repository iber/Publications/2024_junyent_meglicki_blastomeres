% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

function run_model()

T = readtable('cellcounts_human.csv');

NpICM = T.ICM_GFPp; % total number of GFP+ cells in ICM
NpTE = T.TE_GFPp; % total number of GFP+ cells in TE

NnICM = T.ICM_GFPn; % total number of GFP- cells in ICM
NnTE = T.TE_GFPn; % total number of GFP- cells in TE

Np = NpICM + NpTE; % total number of GFP+ cells
Nn = NnICM + NnTE; % total number of GFP- cells
NICM = NpICM + NnICM; % total number of cells in ICM
NTE = NpTE + NnTE; % total number of cells in TE
N = NICM + NTE; % total number of cells

pos = Np ./ N; % fraction of GFP+ cells in whole embryo
pos_TE = NpTE ./ NTE; % fraction of GFP+ cells in TE
pos_ICM = NpICM ./ NICM; % fraction of GFP+ cells in ICM
ICM_size = NICM ./ N; % fraction of cells in the ICM

Nembryos = numel(N); % number of embryos

arr = 8 / Nembryos; % fraction of embryos with arrested cells

Nruns = 1e4; % number of repetition runs
arrest = 0.065; % mean fraction of cells arresting
death = 0.044; % mean fraction of cells dying
Nin_min = [0 0 1 0 0 zeros(1,15)]; % minimum number of cells internalizing
Nin_max = [0 0 3 2 1 zeros(1,15)]; % maximum number of cells internalizing
bias = 0.5; % balance between GFP+ and GFP- cells that internalize
symbal = 0; % asymmetric/symmetric internalization balance
compint = true; % compensate for too few or too many cells already internalized?

FontSize = 18;

rng("default") % for reproducibility

DICM = NaN(Nembryos,1);
DTE = NaN(Nembryos,1);
for i = 1:Nembryos
    if (NpICM(i) > NnICM(i)) % GFP+ cells dominant in the ICM
        DICM(i) = NpICM(i) / NICM(i);
        DTE(i)  = NpTE(i)  / NTE(i);
    else % GFP- cells dominant in the ICM
        DICM(i) = NnICM(i) / NICM(i);
        DTE(i)  = NnTE(i)  / NTE(i);
    end
end

% sort the whole-embryo percentages of GFP+ cells and the percentages of ICM-dominant cells 
pos = sort(pos);
DTE = sort(DTE);
DICM = sort(DICM);

% simulate blastocyst development
tic
[pos_mod, pos_TE_mod, pos_ICM_mod, arr_mod, ICM_size_mod, DICM_mod, DTE_mod, Npos_ICM_16_mod, Nneg_ICM_16_mod, dead_mod] = ...
    stochastic_blastocyst(N, arrest, death, Nin_min, Nin_max, bias, symbal, compint, Nruns, true);
toc

% sort the whole-embryo percentages of GFP+ cells and the percentages of ICM-dominant cells 
for j = 1:Nruns
    pos_mod(:,j)  = sort(pos_mod(:,j));
    DICM_mod(:,j) = sort(DICM_mod(:,j));
    DTE_mod(:,j)  = sort(DTE_mod(:,j));
end

% total percentage of GFP+ cells
pos_mod_mean = mean(pos_mod,2);
pos_mod_SD   = std(pos_mod,0,2);

% percentage of arrested cells
arr_mod_mean = nanmean(arr_mod);
arr_mod_SD   = nanstd(arr_mod);

% percentage of ICM-dominant cells in TE
DTE_mod_mean = nanmean(DTE_mod,2);
DTE_mod_SD   = nanstd(DTE_mod,0,2);

% percentage of ICM-dominant cells in ICM
DICM_mod_mean = nanmean(DICM_mod,2);
DICM_mod_SD   = nanstd(DICM_mod,0,2);

% percentage of dead cells in blastocyst
dead_mod_mean = 100 * mean(dead_mod,'all')

close all

%% Figure 1: box chart for the relative size of the ICM
figure('Position', [0 0 300 500], 'Name', 'ICM size')
model = [repmat({'Human embryos'}, Nembryos, 1); repmat({'Statistical model'}, Nembryos*Nruns, 1)];
tissues = categorical([repmat({'-'}, Nembryos, 1); repmat({'-'}, Nembryos*Nruns, 1)], {'-'});
boxchart(tissues, 100 * [ICM_size; ICM_size_mod(:)], 'GroupByColor', model, 'BoxFaceAlpha', 1, 'jitterOutliers', 'on', 'BoxEdgeColor', 'black')
ylabel('Relative ICM size (% of cells)')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
box on
legend('show', 'Location', 'northwest')
ylim([0 50])
yticks(0:10:50);
hold on
nbars = 2;
groupwidth = min(0.8, nbars/(nbars + 1.5));

% p-value
[~, pICM_size] = kstest2(ICM_size, ICM_size_mod(:));
for i = 1:numel(pICM_size)
    line(i - groupwidth/2 + ([0.5 2.5]*2-1) * groupwidth / (2*nbars), [39 39], 'Color', 'black', 'LineWidth', 2, 'HandleVisibility', 'off')
    text(i, 42, sprintf('P=%.2f', pICM_size(i)), 'FontSize', FontSize, 'HorizontalAlignment', 'center')
end

name = 'ICM_size';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.ICM_size = 100 * ICM_size(:);
writetable(T, [name '_embryos.csv']);
T = table();
T.ICM_size = 100 * ICM_size_mod(:);
writetable(T, [name '_model.csv']);

%% Figure 2: bar chart for the clonal imbalance
figure('Position', [0 0 400 500], 'Name', 'Clonal imbalance')
imbal_TE = mean(DTE <= 0.4 | 0.6 <= DTE);
imbal_ICM = mean(0.6 <= DICM);
imbal_TE_mod = mean(DTE_mod <= 0.4 | 0.6 <= DTE_mod, 1);
imbal_ICM_mod = mean(0.6 <= DICM_mod, 1);
imbal_TE_mod_mean = mean(imbal_TE_mod);
imbal_ICM_mod_mean = mean(imbal_ICM_mod);
imbal_TE_mod_SD = std(imbal_TE_mod);
imbal_ICM_mod_SD = std(imbal_ICM_mod);
hb = bar(100 * [imbal_TE imbal_TE_mod_mean; imbal_ICM imbal_ICM_mod_mean]);
set(hb, {'DisplayName'}, {'Human embryos'; 'Statistical model'});
legend show
ylabel('% of embryos with high imbalance (≤40% or ≥60%)')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
hold on
nbars = 2;
groupwidth = min(0.8, nbars/(nbars + 1.5));
x = (1:2) - groupwidth/2 + 3 * groupwidth / (2*nbars);
errorbar(x, 100*[imbal_TE_mod_mean imbal_ICM_mod_mean], 100*[imbal_TE_mod_SD imbal_ICM_mod_SD], 'k.', 'LineWidth', 1, 'HandleVisibility', 'off');
ylim([0 120])
xticklabels({'TE', 'ICM'})

% p-value for each tissue
p = NaN(2,1);
p(1) = sample_pvalue(imbal_TE, imbal_TE_mod);
p(2) = sample_pvalue(imbal_ICM, imbal_ICM_mod);
for i = 1:numel(p)
    line(i - groupwidth/2 + ([0.5 2.5]*2-1) * groupwidth / (2*nbars), [95 95], 'Color', 'black', 'LineWidth', 2, 'HandleVisibility', 'off')
    text(i, 99, sprintf('P=%.2f', p(i)), 'FontSize', FontSize, 'HorizontalAlignment', 'center')
end

name = 'imbalance_percentage';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.imbalance = 100 * [imbal_TE; imbal_ICM];
T.imbalance_model_mean = 100 * [imbal_TE_mod_mean; imbal_ICM_mod_mean];
T.imbalance_model_SD = 100 * [imbal_TE_mod_SD; imbal_ICM_mod_SD];
T.p_value = p;
writetable(T, [name '.csv']);

%% Figure 3: bar chart for the percentage of GFP+ cells
figure('Position', [0 0 1200 600], 'Name', 'Distribution of GFP+ cells by embryo')
hb = bar(100 * [pos pos_mod_mean]);
set(hb, {'DisplayName'}, {'Human embryos'; 'Statistical model'});
legend('show', 'Location', 'northwest')
xlabel('Embryo')
ylabel('Whole embryo percentage of GFP+ cells')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
hold on
nbars = 2;
groupwidth = min(0.8, nbars/(nbars + 1.5));
x = (1:Nembryos) - groupwidth/2 + 3 * groupwidth / (2*nbars);
errorbar(x, 100*pos_mod_mean, 100*pos_mod_SD, 'k.', 'LineWidth', 1, 'HandleVisibility', 'off');
ylim([0 120])
xticks(1:Nembryos)

% p-value for each embryo
p = NaN(Nembryos,1);
for i = 1:Nembryos
    p(i) = sample_pvalue(pos(i), pos_mod(i,:));
    line(i - groupwidth/2 + ([0.5 2.5]*2-1) * groupwidth / (2*nbars), [100 100], 'Color', 'black', 'LineWidth', 2, 'HandleVisibility', 'off')
    text(i, 104, stars(p(i), Nruns), 'FontSize', FontSize, 'HorizontalAlignment', 'center')
end

name = 'GFPplus_percentage';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.GFPplus = 100 * pos;
T.GFPplus_model_mean = 100 * pos_mod_mean;
T.GFPplus_model_SD = 100 * pos_mod_SD;
T.p_value = p;
writetable(T, [name '.csv']);

%% Figure 4: box chart for the distribution of GFP+ cells
figure('Position', [0 0 600 500], 'Name', 'Total distribution of GFP+ cells')
model = [repmat({'Human embryos'}, 3*Nembryos, 1); repmat({'Statistical model'}, 3*Nembryos*Nruns, 1)];
tissues = categorical([repmat({'Whole embryo'}, Nembryos, 1); repmat({'TE'}, Nembryos, 1); repmat({'ICM'}, Nembryos, 1); repmat({'Whole embryo'}, Nembryos*Nruns, 1); repmat({'TE'}, Nembryos*Nruns, 1); repmat({'ICM'}, Nembryos*Nruns, 1)], {'Whole embryo', 'TE', 'ICM'});
boxchart(tissues, 100 * [pos; pos_TE; pos_ICM; pos_mod(:); pos_TE_mod(:); pos_ICM_mod(:)], 'GroupByColor', model, 'BoxFaceAlpha', 1, 'jitterOutliers', 'on', 'BoxEdgeColor', 'black')
ylabel('GFP+ cells (%)')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
box on
legend('show', 'Location', 'southeast')
ylim([0 115])
yticks(0:25:100);
hold on

% p-value for each tissue
p = NaN(3,1);
[~, p(1)] = kstest2(pos, pos_mod(:));
[~, p(2)] = kstest2(pos_TE, pos_TE_mod(:));
[~, p(3)] = kstest2(pos_ICM, pos_ICM_mod(:));
for i = 1:numel(p)
    line(i - groupwidth/2 + ([0.5 2.5]*2-1) * groupwidth / (2*nbars), [105 105], 'Color', 'black', 'LineWidth', 2, 'HandleVisibility', 'off')
    text(i, 109, sprintf('P=%.2f', p(i)), 'FontSize', FontSize, 'HorizontalAlignment', 'center')
end
ppos = p(1);

name = 'GFPplus_distribution';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.whole = 100 * pos(:);
T.TE = 100 * pos_TE(:);
T.ICM = 100 * pos_ICM(:);
writetable(T, [name '_embryos.csv']);
T = table();
T.whole = 100 * pos_mod(:);
T.TE = 100 * pos_TE_mod(:);
T.ICM = 100 * pos_ICM_mod(:);
writetable(T, [name '_model.csv']);

%% Figure 5: P-P diagram for the percentage of GFP+ cells in the whole embryo
figure('Position', [0 0 400 500], 'Name', 'P-P plot of GFP+ cells in the whole embryo')
errorbar(100*pos, 100*pos_mod_mean, 100*pos_mod_SD, 'bo-', 'LineWidth', 1, 'MarkerFaceColor', 'b')
line([0 100], [0 100], 'Color', 'k', 'LineWidth', 1)
xlim([0 100])
ylim([0 100])
xlabel('Observed percentage of GFP+ cells')
ylabel('Expected percentage of GFP+ cells')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
axis equal image
xticks(0:20:100)
yticks(0:20:100)

% goodness-of-fit statistics
[R2, chi2_red] = gof(pos_mod_mean, pos, pos_mod_SD, 0);
text(5, 90, {['R^2 = ' sprintf('%0.2f', round(R2,2))], ['\chi^2_{red} = ' sprintf('%0.2f', round(chi2_red,2))]}, 'FontSize', FontSize)

name = 'PPplot_GFPplus';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.GFPplus = 100 * pos;
T.GFPplus_model_mean = 100 * pos_mod_mean;
T.GFPplus_model_SD = 100 * pos_mod_SD;
writetable(T, [name '.csv']);

%% Figure 6: box chart for the distribution of ICM-dominant cells
figure('Position', [0 0 450 500], 'Name', 'Distribution of ICM-dominant cells')
model = [repmat({'Human embryos'}, 2*Nembryos, 1); repmat({'Statistical model'}, 2*Nembryos*Nruns, 1)];
tissues = categorical([repmat({'TE'}, Nembryos, 1); repmat({'ICM'}, Nembryos, 1); repmat({'TE'}, Nembryos*Nruns, 1); repmat({'ICM'}, Nembryos*Nruns, 1)], {'TE', 'ICM'});
boxchart(tissues, 100 * [DTE; DICM; DTE_mod(:); DICM_mod(:)], 'GroupByColor', model, 'BoxFaceAlpha', 1, 'jitterOutliers', 'on', 'BoxEdgeColor', 'black')
ylabel('ICM dominant contribution (% of cells)')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
box on
legend('show', 'Location', 'southeast')
ylim([0 115])
yticks(0:25:100);
hold on

% p-value for each tissue
p = NaN(2,1);
[~, p(1)] = kstest2(DTE, DTE_mod(:));
[~, p(2)] = kstest2(DICM, DICM_mod(:));
for i = 1:numel(p)
    line(i - groupwidth/2 + ([0.5 2.5]*2-1) * groupwidth / (2*nbars), [105 105], 'Color', 'black', 'LineWidth', 2, 'HandleVisibility', 'off')
    text(i, 109, sprintf('P=%.2f', p(i)), 'FontSize', FontSize, 'HorizontalAlignment', 'center')
end
pDTE = p(1);
pDICM = p(2);

name = 'TE_ICM_dominant';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.TE = 100 * DTE_mod(:);
T.ICM = 100 * DICM_mod(:);
writetable(T, [name '.csv']);

%% Figure 7: cumulative distribution functions of the ICM-dominant cells in the TE
figure('Position', [0 0 1200 500], 'Name', 'CDF of ICM-dominant cells')
subplot(1,2,1)
CDF_DTE = cdfplot(100 * DTE);
hold on
CDF_DTE_mod = cdfplot(100 * DTE_mod(:));
legend('show', 'Location', 'northwest')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
set(CDF_DTE, 'LineWidth', 2, 'DisplayName', 'Human embryos');
set(CDF_DTE_mod, 'LineWidth', 2, 'DisplayName', 'Statistical model');
title('TE')
xlabel('ICM dominant contribution (% of cells)')
ylabel('Cumulative distribution function')
box on

% p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of ICM-dominant cells
[~, p] = kstest2(DTE, DTE_mod(:));
text(98, 0.15, 'Kolmogorov-Smirnov test:', 'FontSize', FontSize, 'HorizontalAlignment', 'right')
text(98, 0.05, ['P = ' sprintf('%.2f', p)], 'FontSize', FontSize, 'HorizontalAlignment', 'right')

%% cumulative distribution functions of the ICM-dominant cells in the ICM
subplot(1,2,2)
CDF_DICM = cdfplot(100 * DICM);
hold on
CDF_DICM_mod = cdfplot(100 * DICM_mod(:));
legend('show', 'Location', 'northwest')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
set(CDF_DICM, 'LineWidth', 2, 'DisplayName', 'Human embryos');
set(CDF_DICM_mod, 'LineWidth', 2, 'DisplayName', 'Statistical model');
title('ICM')
xlabel('ICM dominant contribution (% of cells)')
ylabel('Cumulative distribution function')
box on

% p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of ICM-dominant cells
[~, p] = kstest2(DICM, DICM_mod(:));
text(98, 0.15, 'Kolmogorov-Smirnov test:', 'FontSize', FontSize, 'HorizontalAlignment', 'right')
text(98, 0.05, ['P = ' sprintf('%.2f', p)], 'FontSize', FontSize, 'HorizontalAlignment', 'right')

name = 'ICM_dominant_CDF';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
h = findobj(CDF_DTE, 'Type', 'line');
T.DTE = h.XData';
T.DTE(1) = 0;
T.DTE(end) = 100;
T.DTE_CDF = 100 * h.YData';
writetable(T, [name '_TE_embryos.csv']);

T = table();
h = findobj(CDF_DICM, 'Type', 'line');
T.DICM = h.XData';
T.DICM(1) = 50;
T.DICM(end) = 100;
T.DICM_CDF = 100 * h.YData';
writetable(T, [name '_ICM_embryos.csv']);

T = table();
h = findobj(CDF_DTE_mod, 'Type', 'line');
T.DTE = h.XData';
T.DTE(1) = 0;
T.DTE(end) = 100;
T.DTE_CDF = 100 * h.YData';
writetable(T, [name '_TE_model.csv']);

T = table();
h = findobj(CDF_DICM_mod, 'Type', 'line');
T.DICM = h.XData';
T.DICM(1) = 50;
T.DICM(end) = 100;
T.DICM_CDF = 100 * h.YData';
writetable(T, [name '_ICM_model.csv']);

%% Figure 8: P-P diagram for the ICM-dominant cells in the TE
figure('Position', [0 0 900 500], 'Name', 'P-P plot of ICM-dominant cells')
subplot(1,2,1)
errorbar(100*DTE, 100*DTE_mod_mean, 100*DTE_mod_SD, 'bo-', 'LineWidth', 1, 'MarkerFaceColor', 'b')
line([0 100], [0 100], 'Color', 'k', 'LineWidth', 1)
title('TE')
xlabel('Observed % of ICM dominant cells')
ylabel('Expected % of ICM dominant cells')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
axis equal image
xticks(0:20:100)
yticks(0:20:100)

% goodness-of-fit statistics
[R2, chi2_red] = gof(DTE_mod_mean, DTE, DTE_mod_SD, 0);
text(5, 90, {['R^2 = ' sprintf('%0.2f', round(R2,2))], ['\chi^2_{red} = ' sprintf('%0.2f', round(chi2_red,2))]}, 'FontSize', FontSize)

xlim([0 100])
ylim([0 100])

%% P-P diagram for the ICM-dominant cells in the ICM
subplot(1,2,2)
errorbar(100*DICM, 100*DICM_mod_mean, 100*DICM_mod_SD, 'bo-', 'LineWidth', 1, 'MarkerFaceColor', 'b')
line([50 100], [50 100], 'Color', 'k', 'LineWidth', 1)
title('ICM')
xlabel('Observed % of ICM dominant cells')
ylabel('Expected % of ICM dominant cells')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
axis equal image
xticks(50:10:100)
yticks(50:10:100)

% goodness-of-fit statistics
[R2, chi2_red] = gof(DICM_mod_mean, DICM, DICM_mod_SD, 0);
text(55, 95, {['R^2 = ' sprintf('%0.2f', round(R2,2))], ['\chi^2_{red} = ' sprintf('%0.2f', round(chi2_red,2))]}, 'FontSize', FontSize)

xlim([50 100])
ylim([50 100])

name = 'PPplot_ICM_dominant';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.TE = 100 * DTE;
T.TE_model_mean = 100 * DTE_mod_mean;
T.TE_model_SD = 100 * DTE_mod_SD;
T.ICM = 100 * DICM;
T.ICM_model_mean = 100 * DICM_mod_mean;
T.ICM_model_SD = 100 * DICM_mod_SD;
writetable(T, [name '.csv'])

%% Figure 9: bar chart for the percentage of arrested cells
figure('Position', [0 0 300 500], 'Name', 'Arrested cells')
hb = bar(100 * [arr arr_mod_mean; nan nan]);
set(hb, {'DisplayName'}, {'Human embryos'; 'Statistical model'});
legend('show', 'Location', 'northwest')
ylabel('Embryos with arrested blastomeres (%)')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
hold on
nbars = 2;
groupwidth = min(0.8, nbars/(nbars + 1.5));
x = (1:1) - groupwidth/2 + 3 * groupwidth / (2*nbars);
errorbar(x, 100 * arr_mod_mean, 100 * arr_mod_SD, 'k.', 'LineWidth', 1, 'HandleVisibility', 'off');
xlim([0.5 1.5])
ylim([0 100])
xticks([])

% p-value
p = sample_pvalue(arr, arr_mod);
for i = 1:numel(p)
    line(i - groupwidth/2 + ([0.5 2.5]*2-1) * groupwidth / (2*nbars), [74 74], 'Color', 'black', 'LineWidth', 2, 'HandleVisibility', 'off')
    text(i, 77, sprintf('P=%.2f', p(i)), 'FontSize', FontSize, 'HorizontalAlignment', 'center')
end

name = 'arrested_percentage';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.arrested = 100 * arr;
T.arrested_model_mean = 100 * arr_mod_mean;
T.arrested_model_SD = 100 * arr_mod_SD;
T.p_value = p;
writetable(T, [name '.csv'])

%% Figure 10: correlation between ICM composition at the 16-cell stage and the final blastocyst
pos_ICM_16_mod = Npos_ICM_16_mod ./ (Npos_ICM_16_mod + Nneg_ICM_16_mod); % fraction of GFP+ cells in ICM at 16-cell stage
figure('Position', [0 0 600 500], 'Name', 'Importance of the ICM composition at the 16-cell stage')
boxchart(100 * pos_ICM_16_mod(:), 100 * pos_ICM_mod(:), 'MarkerColor', [0.850 0.325 0.098], 'BoxFaceColor', [0.850 0.325 0.098], 'BoxFaceAlpha', 1, 'jitterOutliers', 'on', 'BoxEdgeColor', 'black', 'BoxWidth', 10)
xlabel('Percentage of GFP+ cells in ICM at 16-cell stage')
ylabel('Percentage of GFP+ cells in ICM in blastocyst')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
xlim([-10 110])
ylim([-10 110])
xticks(100*[0 1 1.5 2 3]/3)
xticklabels({'0', '33.3', '50', '66.7', '100'})
yticks(0:25:100)
box on
hold on
idx = isfinite(pos_ICM_16_mod) & isfinite(pos_ICM_mod);
pos_ICM_16_mod = pos_ICM_16_mod(idx);
pos_ICM_mod = pos_ICM_mod(idx);
fitcoeff = polyfit(100 * pos_ICM_16_mod, 100 * pos_ICM_mod, 1)
plot([0 100], polyval(fitcoeff, [0 100]), 'k-', 'LineWidth', 4)
r_Pearson = corr(pos_ICM_16_mod, pos_ICM_mod, 'type', 'Pearson');
tau_Kendall = corr(pos_ICM_16_mod, pos_ICM_mod, 'type', 'Kendall');
rho_spearman = corr(pos_ICM_16_mod, pos_ICM_mod, 'type', 'Spearman');
text(-5, 75, {['Pearson''s r = ' sprintf('%0.2f', round(r_Pearson,2))], ['Kendall''s \tau = ' sprintf('%0.2f', round(tau_Kendall,2))], ['Spearman''s \rho = ' sprintf('%0.2f', round(rho_spearman,2))]}, 'FontSize', FontSize)

name = 'GFPplus_correlation_model';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.cell16 = 100 * pos_ICM_16_mod;
T.final = 100 * pos_ICM_mod;
writetable(T, [name '.csv'])

% percentage of embryos with a ICM dominant cell type at the 16-cell stage that is the same as at the blastocyst stage
has_DICM_16 = find(pos_ICM_16_mod ~= 0.5);
pct_same_DICM = 100 * mean(sign(pos_ICM_16_mod(has_DICM_16)-0.5) == sign(pos_ICM_mod(has_DICM_16)-0.5))

%% Figure 11: prediction of ICM composition in blastocyst from embryoscope embryos at 16-cell stage (1/2)
Nemb = [16 11 2 10 14]; % embryoscope data
NICMD  = [1 2 3 2 1];
NICMND = [0 0 0 1 1];
idx_all = [];
individual_DICM_mod = NaN(max(Nemb) * Nruns, numel(Nemb));
for i = 1:numel(Nemb)
    idx = find((Npos_ICM_16_mod == NICMD(i) & Nneg_ICM_16_mod == NICMND(i)) | (Npos_ICM_16_mod == NICMND(i) & Nneg_ICM_16_mod == NICMD(i)));
    idx = randsample(idx, Nemb(i) * Nruns, true);
    idx_all = [idx_all; idx];
    individual_DICM_mod(1:numel(idx),i) = DICM_mod(idx);
end
figure('Position', [0 0 600 500], 'Name', 'Distribution of ICM-dominant cells from embryoscope data')
boxchart(100 * individual_DICM_mod, 'MarkerColor', [0.850 0.325 0.098], 'BoxFaceColor', [0.850 0.325 0.098], 'BoxFaceAlpha', 1, 'jitterOutliers', 'on', 'BoxEdgeColor', 'black', 'BoxWidth', 0.5)
box on
xticklabels(arrayfun(@(i) [num2str(NICMD(i)) ':' num2str(NICMND(i))], 1:numel(Nemb), 'UniformOutput', false))
xlabel('ICM composition at 16-cell stage')
ylabel('ICM dominant contribution in ICM (% of cells)')
ylim([0 115])
set(gca, 'FontSize', FontSize, 'LineWidth', 1)

name = 'ICM_dominant_embryoscope';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
for i = 1:numel(Nemb)
    T.(['DICM' num2str(NICMD(i)) num2str(NICMND(i))]) = 100 * individual_DICM_mod(:,i);
end
writetable(T, [name '.csv'])

%% Figure 12: prediction of ICM composition in blastocyst from embryoscope embryos at 16-cell stage (2/2)
figure('Position', [0 0 1200 500], 'Name', 'CDF of ICM-dominant cells predicted from embryoscope data')
subplot(1,2,1)
CDF_DTE = cdfplot(100 * DTE);
hold on
CDF_DTE_mod = cdfplot(100 * DTE_mod(idx_all));
legend('show', 'Location', 'northwest')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
set(CDF_DTE, 'LineWidth', 2, 'DisplayName', 'Blastomere labeling');
set(CDF_DTE_mod, 'LineWidth', 2, 'DisplayName', 'Prediction for embryoscope data');
title('TE at blastocyst stage')
xlabel('ICM dominant contribution (% of cells)')
ylabel('Cumulative distribution function')
box on

% p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of ICM-dominant cells
[~, p] = kstest2(DTE, DTE_mod(idx_all));
text(98, 0.15, 'Kolmogorov-Smirnov test:', 'FontSize', FontSize, 'HorizontalAlignment', 'right')
text(98, 0.05, ['P = ' sprintf('%.2f', p)], 'FontSize', FontSize, 'HorizontalAlignment', 'right')

subplot(1,2,2)
CDF_DICM = cdfplot(100 * DICM);
hold on
CDF_DICM_mod = cdfplot(100 * DICM_mod(idx_all));
legend('show', 'Location', 'northwest')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
set(CDF_DICM, 'LineWidth', 2, 'DisplayName', 'Blastomere labeling');
set(CDF_DICM_mod, 'LineWidth', 2, 'DisplayName', 'Prediction for embryoscope data');
title('ICM at blastocyst stage')
xlabel('ICM dominant contribution (% of cells)')
ylabel('Cumulative distribution function')
box on

% p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of ICM-dominant cells
[~, p] = kstest2(DICM, DICM_mod(idx_all));
text(98, 0.15, 'Kolmogorov-Smirnov test:', 'FontSize', FontSize, 'HorizontalAlignment', 'right')
text(98, 0.05, ['P = ' sprintf('%.2f', p)], 'FontSize', FontSize, 'HorizontalAlignment', 'right')

name = 'ICM_dominant_CDF_embryoscope';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
h = findobj(CDF_DTE_mod, 'Type', 'line');
T.DTE = h.XData';
T.DTE(1) = 0;
T.DTE(end) = 100;
T.DTE_CDF = 100 * h.YData';
writetable(T, [name '_TE_model.csv']);

T = table();
h = findobj(CDF_DICM_mod, 'Type', 'line');
T.DICM = h.XData';
T.DICM(1) = 50;
T.DICM(end) = 100;
T.DICM_CDF = 100 * h.YData';
writetable(T, [name '_ICM_model.csv']);

%% overall model performance
p = [pDTE pDICM pICM_size ppos]'
hmp = 4 / (1/pDTE + 1/pDICM + 1/pICM_size + 1/ppos) % harmonic mean p-value

end