% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

% p: p-value
% N: sample size, which determines the maximum number of stars
function str = stars(p, N)
    txt = {'ns', '*', '**', '***', '****', '*****'};
    s = min(floor(log10(1./(p+eps))), floor(log10(N)));
    s(p >= 0.05) = 0;
    str = txt{s + 1};
end