% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

function [hmp, pDTE, pDICM, pICM_size, ppos] = optimize_arrest()

T = readtable('cellcounts_human.csv');

NpICM = T.ICM_GFPp; % total number of GFP+ cells in ICM
NpTE = T.TE_GFPp; % total number of GFP+ cells in TE

NnICM = T.ICM_GFPn; % total number of GFP- cells in ICM
NnTE = T.TE_GFPn; % total number of GFP- cells in TE

Np = NpICM + NpTE; % total number of GFP+ cells
Nn = NnICM + NnTE; % total number of GFP- cells
NICM = NpICM + NnICM; % total number of cells in ICM
NTE = NpTE + NnTE; % total number of cells in TE
N = NICM + NTE; % total number of cells

pos = Np ./ N; % fraction of GFP+ cells
ICM_size = NICM ./ N; % fraction of cells in the ICM

Nembryos = numel(N); % number of embryos

Nruns = 1e4; % number of repetition runs
death = 0.044; % mean fraction of cells dying
Nin_min = [0 0 1 0 0 zeros(1,15)]; % minimum number of cells internalizing
Nin_max = [0 0 3 2 1 zeros(1,15)]; % maximum number of cells internalizing
bias = 0.5; % balance between GFP+ and GFP- cells that internalize
symbal = 0; % asymmetric/symmetric internalization balance
compint = true; % compensate for too few or too many cells already internalized?

% parameters to screen
arrest = 0:0.005:0.25; % mean fraction of cells arresting

% ICM-dominant cell types in TE and ICM
DICM = NaN(Nembryos,1);
DTE = NaN(Nembryos,1);
for i = 1:Nembryos
    if (NpICM(i) > NnICM(i)) % GFP+ cells dominant in the ICM
        DICM(i) = NpICM(i) / NICM(i);
        DTE(i)  = NpTE(i)  / NTE(i);
    else % GFP- cells dominant in the ICM
        DICM(i) = NnICM(i) / NICM(i);
        DTE(i)  = NnTE(i)  / NTE(i);
    end
end

% screen for the best internalization parameters
N1 = numel(arrest);
pDTE = NaN(N1,1);
pDICM = pDTE;
pICM_size = pDTE;
ppos = pDTE;
tic
parfor i1 = 1:N1
    % simulate blastocyst development
    [pos_mod, ~, ~, ~, ICM_size_mod, DICM_mod, DTE_mod] = stochastic_blastocyst(N, arrest(i1), death, Nin_min, Nin_max, bias, symbal, compint, Nruns, false);
    
    % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of ICM-dominant cells
    [~, pDTE(i1)] = kstest2(DTE, DTE_mod(:));
    [~, pDICM(i1)] = kstest2(DICM, DICM_mod(:));
    
    % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted fraction of cells that ended up in the ICM
    [~, pICM_size(i1)] = kstest2(ICM_size, ICM_size_mod(:));
    
    % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of GFP+ cells in whole embryo
    [~, ppos(i1)] = kstest2(pos, pos_mod(:));
end
toc

% find and print the optimum
hmp = 4 ./ (1./pDTE + 1./pDICM + 1./pICM_size + 1./ppos); % harmonic mean p-value
[~,i] = max(hmp, [], 'all');
[pDTE(i) pDICM(i) pICM_size(i) ppos(i)]'
hmp(i)
arrest(i)

pDTE = squeeze(pDTE);
pDICM = squeeze(pDICM);
pICM_size = squeeze(pICM_size);
ppos = squeeze(ppos);
hmp = squeeze(hmp);

T = table();
T.arrest = arrest';
T.pDTE = pDTE(:);
T.pDICM = pDICM(:);
T.pICM_size = pICM_size(:);
T.ppos = ppos(:);
T.hmp = hmp(:);
writetable(T, 'optimum_arrest.csv')

end