% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

function [hmp, pDTE, pDICM, pICM_size, ppos] = optimize_arrest_stage()

T = readtable('cellcounts_human.csv');

NpICM = T.ICM_GFPp; % total number of GFP+ cells in ICM
NpTE = T.TE_GFPp; % total number of GFP+ cells in TE

NnICM = T.ICM_GFPn; % total number of GFP- cells in ICM
NnTE = T.TE_GFPn; % total number of GFP- cells in TE

Np = NpICM + NpTE; % total number of GFP+ cells
Nn = NnICM + NnTE; % total number of GFP- cells
NICM = NpICM + NnICM; % total number of cells in ICM
NTE = NpTE + NnTE; % total number of cells in TE
N = NICM + NTE; % total number of cells

pos = Np ./ N; % fraction of GFP+ cells
ICM_size = NICM ./ N; % fraction of cells in the ICM

Nembryos = numel(N); % number of embryos

arr = 8 / Nembryos; % fraction of embryos with arrested cells

Nruns = 1e4; % number of repetition runs
death = 0.044; % mean fraction of cells dying
Nin_min = [0 0 1 0 0 zeros(1,15)]; % minimum number of cells internalizing
Nin_max = [0 0 3 2 1 zeros(1,15)]; % maximum number of cells internalizing
symbal = 0; % asymmetric/symmetric internalization balance
compint = true; % compensate for too few or too many cells already internalized?

% parameters to screen
gen = 2:6; % generation in which cell arrest occurs
arrest = 0.2; % mean fraction of cells arresting
bias = [0.5 0.7]; % balance between GFP+ and GFP- cells that internalize

% ICM-dominant cell types in TE and ICM
DICM = NaN(Nembryos,1);
DTE = NaN(Nembryos,1);
for i = 1:Nembryos
    if (NpICM(i) > NnICM(i)) % GFP+ cells dominant in the ICM
        DICM(i) = NpICM(i) / NICM(i);
        DTE(i)  = NpTE(i)  / NTE(i);
    else % GFP- cells dominant in the ICM
        DICM(i) = NnICM(i) / NICM(i);
        DTE(i)  = NnTE(i)  / NTE(i);
    end
end

% returns the difference between predicted and observed fraction of embryos with arrested blastomeres
function darr = f(a,g)
    [~, ~, ~, arr_mod] = stochastic_blastocyst(N, [zeros(1,g-1) a zeros(1,20)], death, Nin_min, Nin_max, 0.5, symbal, compint, Nruns, false);
    darr = mean(arr_mod,'all') - arr;
end
    
% screen for the best internalization parameters
N1 = numel(gen);
N2 = numel(bias);
pDTE = NaN(N1,N2);
pDICM = pDTE;
pICM_size = pDTE;
ppos = pDTE;
delete(findall(0,'type','figure','tag','TMWWaitbar'))
wb = waitbar(0, '0 %', 'Name', 'Progress');
tic
for i1 = 1:N1
    % find the cell arrest rate that reproduces the observed fraction of embryos with arrested cells
    arrest(gen(i1)) = regula_falsi(@(a) f(a,gen(i1)),0,arrest(gen(i1)-1),1e-4);

    parfor i2 = 1:N2
        % simulate blastocyst development
        [pos_mod, ~, ~, ~, ICM_size_mod, DICM_mod, DTE_mod] = stochastic_blastocyst(N, [zeros(1,gen(i1)-1) arrest(gen(i1)) zeros(1,20)], death, Nin_min, Nin_max, bias(i2), symbal, compint, Nruns, false);
        
        % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of ICM-dominant cells
        [~, pDTE(i1,i2)] = kstest2(DTE, DTE_mod(:));
        [~, pDICM(i1,i2)] = kstest2(DICM, DICM_mod(:));
        
        % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted fraction of cells that ended up in the ICM
        [~, pICM_size(i1,i2)] = kstest2(ICM_size, ICM_size_mod(:));
        
        % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of GFP+ cells in whole embryo
        [~, ppos(i1,i2)] = kstest2(pos, pos_mod(:));
    end

    progress = i1 / N1;
    waitbar(progress, wb, sprintf('%i %% (time left: %s)', floor(100 * progress), duration(0,0,toc*(1-progress)/progress)));
end
toc
close(wb)

hmp = 4 ./ (1./pDTE + 1./pDICM + 1./pICM_size + 1./ppos); % harmonic mean p-value

T = table();
T.gen = gen';
T.arrest = 100 * arrest(gen)';
for i2 = 1:N2
    T.(['hmp_bias' num2str(100*bias(i2))]) = hmp(:,i2);
end
writetable(T, 'optimum_arrest_stage.csv')

end