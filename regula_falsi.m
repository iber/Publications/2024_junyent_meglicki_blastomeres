% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

% finds a zero of a scalar function f between a and b, for a<b and f(a)*f(b)<0
function c = regula_falsi(f,a,b,tol)
    fa = f(a);
    fb = f(b);
    while true
        c = (a * fb - b * fa) / (fb - fa);
        if c - a < tol || b - c < tol
            return
        end
        fc = f(c);
        if fa * fc > 0
            a = c;
            fa = fc;
        else
            b = c;
            fb = fc;
        end
    end
end