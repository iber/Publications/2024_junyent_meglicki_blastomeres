% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

function [pos, pos_TE, pos_ICM, arrested, ICM_size, DICM, DTE, Npos_ICM_16, Nneg_ICM_16, dead] = stochastic_blastocyst(Ncells, arrest, death, Nin_min, Nin_max, bias, symbal, compint, Nruns, progressbar)

%% ARGUMENTS
% Ncells      : list of total number of cells in each embryo
% arrest      : list of mean probabilities of non-arrested cells to arrest after each division generation
% death       : list of mean probabilities of internal cells to die after each division generation
% Nin_min     : list of minimum numbers of cells that internalize after each division generation
% Nin_max     : list of maximum numbers of cells that internalize after each division generation
% bias        : balance between GFP+ and GFP- cells for internalization, 0.5 = equal propensity
% symbal      : asymmetric/symmetric internalization balance, 0 = only one daughter can internalize, 1 = both daughters can
% compint     : boolean flag indicating whether for too few or many cells internalized should be compensated in later generations
% Nruns       : number of repetitions
% progressbar : boolean flag indicating whether a progress bar should be shown

%% RETURN VALUES
% pos         : matrix of size Nembryos x Nruns containing the fraction of GFP+ cells in the whole embryo
% pos_TE      : matrix of size Nembryos x Nruns containing the fraction of GFP+ cells in the TE
% pos_ICM     : matrix of size Nembryos x Nruns containing the fraction of GFP+ cells in the ICM
% arrested    : vector of size Nruns containing the fraction of embryos with arrested cells
% ICM_size    : matrix of size Nembryos x Nruns containing the fraction of cells ending up in the ICM
% DICM        : matrix of size Nembryos x Nruns containing the fraction of the ICM-dominant cell type in the ICM
% DTE         : matrix of size Nembryos x Nruns containing the fraction of the ICM-dominant cell type in the TE
% Npos_ICM_16 : matrix of size Nembryos x Nruns containing the number of GFP+ cells in the ICM at the 16-cell stage
% Nneg_ICM_16 : matrix of size Nembryos x Nruns containing the number of GFP- cells in the ICM at the 16-cell stage
% dead        : matrix of size Nembryos x Nruns containing the fraction of dead cells in the whole embryo

if numel(arrest) == 1
    arrest = [0 arrest arrest zeros(1,17)]; % by default, apply cell arrest at 4 and 8 cell stages
end

if numel(death) == 1
    death = [0 0 0 0 0 death*ones(1,16)]; % by default, apply cell death from 64 cells onward
end

if progressbar
    wb = waitbar(0, '0 %', 'Name', 'Progress');
end

Nin_sum_target = cumsum(Nin_min + Nin_max) / 2; % expected number of internalized cells after each division generation

Nembryos = numel(Ncells); % number of embryos
pos = NaN(Nembryos,Nruns); % fraction of GFP+ cells in the whole embryo
pos_TE = NaN(Nembryos,Nruns); % fraction of GFP+ cells in the TE
pos_ICM = NaN(Nembryos,Nruns); % fraction of GFP+ cells in the ICM
arrested = zeros(Nruns,1); % fraction of arrested cells
ICM_size = NaN(Nembryos,Nruns); % fraction of cells in ICM
DICM = NaN(Nembryos,Nruns); % fraction of ICM-dominant cells in ICM
DTE = NaN(Nembryos,Nruns); % fraction of ICM-dominant cells in TE
Npos_ICM_16 = NaN(Nembryos,Nruns); % number of GFP+ cells in the ICM at the 16-cell stage
Nneg_ICM_16 = NaN(Nembryos,Nruns); % number of GFP- cells in the ICM at the 16-cell stage
dead = NaN(Nembryos,Nruns); % fraction of dead cells in the whole embryo

for j = 1:Nruns
    for i = 1:Nembryos
        % bias either GFP+ or GFP- cells
        b = bias;
        if rand < 0.5
            b = 1 - b;
        end

        % 2-cell stage, different cell types
        GFP = [false; true];
        arr = [false; false]; % arrested?
        TE  = [true; true]; % both cells start in the TE until the ICM emerges
        gen = 1; % division generation
        Nin_sum = 0; % number of cells internalized so far
        dead(i,j) = 0;
        fails = 0;
        
        while numel(GFP) < Ncells(i)
            % all non-arrested cells divide
            div = ~arr; % all non-arested cells divide
            NmTE = sum(TE); % number of mother cells in the TE
            GFP = [GFP; GFP(div)];
            arr = [arr; arr(div)];
            TE  = [TE; TE(div)];

            % cell internalization
            if Nin_max(gen) > 0
                can = find(TE);
                NdTE = numel(can); % number of daughter cells in the TE
                can = can([1:NmTE NmTE+randsample(NdTE-NmTE, binornd(NdTE-NmTE, symbal))']); % candidates for internalization
                Nin_diff = Nin_sum - Nin_sum_target(gen-1); % accumulated difference of internalized cells
                Nin = floor(Nin_min(gen) - compint * Nin_diff + rand * (Nin_max(gen) + 1 - Nin_min(gen))); % number of cells internalizing
                Nin = max(min(Nin, numel(can)), 0);
                weights = b * GFP(can) + (1-b) * ~GFP(can); % introduce selection bias
                rin = datasample(can, Nin, 'Replace', false, 'Weights', weights); % random internalizers
                Nin_sum = Nin_sum + numel(rin); % count how many cells internalized so far
                TE(rin) = false;
            end

            gen = gen + 1;

            % record the ICM composition at the 16-cell stage
            if gen == 4
                Npos_ICM_16(i,j) = sum( GFP(~TE));
                Nneg_ICM_16(i,j) = sum(~GFP(~TE));
            end

            % some non-arrested cells arrest
            narr = find(~arr);
            rarr = randsample(numel(narr), binornd(numel(narr), arrest(gen))); % random arresters
            arr(narr(rarr)) = true;

            % some cells die (including those that are arrested)
            rd = randsample(numel(GFP), binornd(numel(GFP), death(gen))); % random diers
            dead(i,j) = dead(i,j) + numel(rd);
            GFP(rd) = [];
            arr(rd) = [];
            TE(rd) = [];
            
            % mare sure there are at least two cells of different type and not all are arrested
            if numel(GFP) < 2 || all(arr) || all(GFP == GFP(1))
                % start over
                GFP = [false; true];
                arr = [false; false];
                TE  = [true; true];
                gen = 1;
                Nin_sum = 0;
                dead(i,j) = 0;

                % if this happens too many times in a row, abort
                fails = fails + 1;
                if fails >= 10
                    if progressbar
                        close(wb)
                    end
                    error("Cell count dropped too low, arrest/death fraction too high")
                end
            end
        end

        % number of dead cells relative to the final number of cells
        dead(i,j) = dead(i,j) / numel(GFP);

        % assume that the final division is incomplete, remove all surplus cells
        keep = randsample(numel(GFP), Ncells(i));
        GFP = GFP(keep);
        arr = arr(keep);
        TE = TE(keep);

        % fraction of cells in ICM
        ICM_size(i,j) = mean(~TE);

        % total fraction of GFP+ cells
        pos(i,j) = mean(GFP);

        % fraction of GFP+ cells in TE and ICM
        ICM = ~TE;
        pos_TE(i,j)  = mean(GFP(TE));
        pos_ICM(i,j) = mean(GFP(ICM));
        
        % clonal composition
        D = mode(GFP(ICM)); % dominant cell type in ICM
        DICM(i,j) = mean(GFP(ICM) == D); % fraction of dominant cell type in ICM
        DTE(i,j)  = mean(GFP(TE ) == D); % fraction of same cell type in the TE

        % number of embryos with arrested cells
        arrested(j) = arrested(j) + any(arr);
    end
    
    if progressbar
        progress = j / Nruns;
        waitbar(progress, wb, sprintf('%i %% (time left: %s)', floor(100 * progress), duration(0,0,toc*(1-progress)/progress)));
    end
end

% convert number of embryos with arrested cells into fraction
arrested = arrested / Nembryos;

if progressbar
    close(wb)
end

end