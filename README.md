# 2024_junyent_meglicki_blastomeres

# Statistical blastocyst development model

Source code accompanying Junyent et al., Blastomeres in the 2-cell human embryo contribute unevenly to embryonic and extraembryonic lineages (2024)

(c) 2023-2024 Dr. Roman Vetter
ETH Zurich

## Contents

* `cellcounts_human.csv` - Human dataset with the number of GPP+/- blastomeres in the trophectoderm (TE) and inner cell mass (ICM) for each of the 22 embryos
* `stochastic_blastocyst.m` - Main model file called by other scripts. Develops a set of blastocysts from the 2-cell stage onward and returns stastistics for them
* `run_model.m` - Main execution script to perform individual model runs (each with many repetitions). Produces several plots.
* `vary_bias.m` - Script to vary the 2-cell internalization bias to generate the plot of the harmonic mean P-value vs. the bias
* `fixed_internalization.m` - Script to alter the cell internalization numbers to generate the plot on the impact on TE/ICM composition
* `ninter_ICM_size.m` - Script to vary cell internalization numbers to generate the plot on the impact on ICM size
* `optimize_death.m` - Varies the cell death rate to find the best-matching value
* `optimize_arrest.m` - Varies the cell arrest rate to find the best-matching value
* `optimize_arrest_stage.m` - Varies the developmental stage in which cells arrest
* `optimize_internalization_stage.m` - Varies the developmental stage in which cells internalize
* `gof.m` - Computes goodness-of-fit statistics for a given model and data
* `regula_falsi.m` - A simple implementation of the Regula Falsi, a root-finding algorithm
* `sample_pvalue.m` - Computes the two-sided P-value for the null hypothesis that a single observation is from the same distribution as a list of values
* `stars.m` - Generates a string with the appropriate number of stars for a given P-value
* `LICENSE` - License file


## Installation, System requirements

The source code was produced and runs with MATLAB version R2023b. No further installations are required, and no further system limitations apply. 


## Usage instructions

The MATLAB scripts can be executed out-of-the-box by just running them in MATLAB. To get started, launch `run_model.m`.


## Input/Output

No manual specification of input or output is required. Input data files, where used, are provided in this repository.
Output figure files and data tables are automatically written to the current working directory.


## Runtime

The main execution script (`run_model.m`) takes about 5 minutes to complete for a single model run with 10,000 repetitions on a normal laptop computer. A progress window is displayed.
The optimization and parameter screening scripts take up to several hours to run. The runtime can be reduced by setting `Nruns` to a lower value, at the cost of poorer statistics.


## License

All source code is released under the 3-Clause BSD license (see `LICENSE` for details).
