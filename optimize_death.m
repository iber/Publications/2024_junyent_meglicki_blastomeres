% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

function optimize_death()

T = readtable('cellcounts_human.csv');

NpICM = T.ICM_GFPp; % total number of GFP+ cells in ICM
NpTE = T.TE_GFPp; % total number of GFP+ cells in TE

NnICM = T.ICM_GFPn; % total number of GFP- cells in ICM
NnTE = T.TE_GFPn; % total number of GFP- cells in TE

Np = NpICM + NpTE; % total number of GFP+ cells
Nn = NnICM + NnTE; % total number of GFP- cells
NICM = NpICM + NnICM; % total number of cells in ICM
NTE = NpTE + NnTE; % total number of cells in TE
N = NICM + NTE; % total number of cells

Nruns = 1e4; % number of repetition runs
arrest = 0.065; % mean fraction of cells arresting
Nin_min = [0 0 1 0 0 zeros(1,15)]; % minimum number of cells internalizing
Nin_max = [0 0 3 2 1 zeros(1,15)]; % maximum number of cells internalizing
bias = 0.5; % balance between GFP+ and GFP- cells that internalize
symbal = 0; % asymmetric/symmetric internalization balance
compint = true; % compensate for too few or too many cells already internalized?

% parameters to screen
death = 0:0.001:0.1; % mean fraction of cells dying

% screen for the best internalization parameters
N1 = numel(death);
dead_mod_mean = NaN(N1,1);
tic
parfor i = 1:N1
    % simulate blastocyst development
    [~, ~, ~, ~, ~, ~, ~, ~, ~, dead_mod] = stochastic_blastocyst(N, arrest, death(i), Nin_min, Nin_max, bias, symbal, compint, Nruns, false);
    
    % fraction of dead cells in whole embryo at blastocyst stage
    dead_mod_mean(i) = mean(dead_mod,'all');
end
toc

T = table();
T.pdeath = 100 * death';
T.dead = 100 * dead_mod_mean;
writetable(T, 'optimum_death.csv')

end