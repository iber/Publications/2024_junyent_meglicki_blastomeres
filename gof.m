% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

% Goodness-of-fit statistics
function [R2, chi2_red] = gof(data, model, sigma, nparam)
    delta = data - model;

    % coefficient of determination
    R2 = 1 - sum(delta.^2) / sum((data - mean(data)).^2);

    % reduced chi-squared statistic
    chi2_red = sum((delta ./ sigma).^2) / (numel(data) - nparam);
end