% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

function ninter_ICM_size()

T = readtable('cellcounts_human.csv');

NpICM = T.ICM_GFPp; % total number of GFP+ cells in ICM
NpTE = T.TE_GFPp; % total number of GFP+ cells in TE

NnICM = T.ICM_GFPn; % total number of GFP- cells in ICM
NnTE = T.TE_GFPn; % total number of GFP- cells in TE

Np = NpICM + NpTE; % total number of GFP+ cells
Nn = NnICM + NnTE; % total number of GFP- cells
NICM = NpICM + NnICM; % total number of cells in ICM
NTE = NpTE + NnTE; % total number of cells in TE
N = NICM + NTE; % total number of cells

ICM_pct = 100 * mean(NICM ./ N) % fraction of cells in the ICM

Nruns = 1e2; % number of repetition runs
arrest = 0.065; % mean fraction of cells arresting
death = 0.044; % mean fraction of cells dying
bias = 0.5; % balance between GFP+ and GFP- cells that internalize
compint = true; % compensate for too few or too many cells already internalized?
symbal = 0; % asymmetric/symmetric internalization balance

% parameters to screen
Nin3 = 0:4; % number of cells internalizing after generation 3
Nin4 = 0:4; % number of cells internalizing after generation 4
Nin5 = 0:1; % number of cells internalizing after generation 5

% screen for the best internalization parameters
N1 = numel(Nin3);
N2 = numel(Nin4);
N3 = numel(Nin5);
ICM_pct_mod = NaN(N1,N2,N3);
delete(findall(0,'type','figure','tag','TMWWaitbar'))
wb = waitbar(0, '0 %', 'Name', 'Progress');
tic
for i1 = 1:N1
    parfor i2 = 1:N2
        for i3 = 1:N3
            % simulate blastocyst development
            [~, ~, ~, ~, ICM_size_mod] = stochastic_blastocyst(N, arrest, death, [0 0 Nin3(i1) Nin3(i2) Nin3(i3) zeros(1,15)], [0 0 Nin3(i1) Nin3(i2) Nin3(i3) zeros(1,15)], bias, symbal, compint, Nruns, false);
            ICM_pct_mod(i1,i2,i3) = 100 * mean(ICM_size_mod,'all');
        end
    end
    progress = i1 / N1;
    waitbar(progress, wb, sprintf('%i %% (time left: %s)', floor(100 * progress), duration(0,0,toc*(1-progress)/progress)));
end
toc
close(wb)

% save heatmap in pgfplots format for plotting in LaTeX
f = fopen('ninter_ICM_size.tex', 'w');
fprintf(f, '\\addplot[matrix plot*, point meta=explicit, m] coordinates {\n');
for i1 = 1:N1
    for i2 = 1:N2
        for i3 = 1:N3
            fprintf(f, '(%g,%g) [%g]\n', Nin3(i1), Nin4(i2)-0.75+i3/N3, ICM_pct_mod(i2,i1,i3)); % note: transposed!
        end
    end
    fprintf(f, '\n');
end
fprintf(f, '};');
fclose(f);

end