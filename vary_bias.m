% Stochastic blastocyst development model
% (c) 2023-2024 Dr. Roman Vetter
% ETH Zurich

function vary_bias()

T = readtable('cellcounts_human.csv');

NpICM = T.ICM_GFPp; % total number of GFP+ cells in ICM
NpTE = T.TE_GFPp; % total number of GFP+ cells in TE

NnICM = T.ICM_GFPn; % total number of GFP- cells in ICM
NnTE = T.TE_GFPn; % total number of GFP- cells in TE

Np = NpICM + NpTE; % total number of GFP+ cells
Nn = NnICM + NnTE; % total number of GFP- cells
NICM = NpICM + NnICM; % total number of cells in ICM
NTE = NpTE + NnTE; % total number of cells in TE
N = NICM + NTE; % total number of cells

pos = Np ./ N; % fraction of GFP+ cells
ICM_size = NICM ./ N; % fraction of cells in the ICM

Nembryos = numel(N); % number of embryos

Nruns = 1e5; % number of repetition runs
arrest = 0.065; % mean fraction of cells arresting
death = 0.044; % mean fraction of cells dying
Nin_min = [0 0 1 0 0 zeros(1,15)]; % minimum number of cells internalizing
Nin_max = [0 0 3 2 1 zeros(1,15)]; % maximum number of cells internalizing
bias = linspace(0.5, 0.98, 25); % balance between GFP+ and GFP- cells that internalize
symbal = 0; % asymmetric/symmetric internalization balance
compint = true; % compensate for too few or too many cells already internalized?

FontSize = 18;

% fraction of ICM-dominant cells in the ICM and TE
DICM = NaN(Nembryos,1);
DTE = NaN(Nembryos,1);
for i = 1:Nembryos
    if (NpICM(i) > NnICM(i)) % GFP+ cells dominant in the ICM
        DICM(i) = NpICM(i) / NICM(i);
        DTE(i)  = NpTE(i)  / NTE(i);
    else % GFP- cells dominant in the ICM
        DICM(i) = NnICM(i) / NICM(i);
        DTE(i)  = NnTE(i)  / NTE(i);
    end
end

% simulate blastocyst development for different stages of cell internalization and biases
pDTE = NaN(numel(bias),1);
pDICM = pDTE;
pICM_size = pDTE;
ppos = pDTE;
tic
parfor i = 1:numel(bias)
    [pos_mod, ~, ~, ~, ICM_size_mod, DICM_mod, DTE_mod] = stochastic_blastocyst(N, arrest, death, Nin_min, Nin_max, bias(i), symbal, compint, Nruns, false);
    
    % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of ICM-dominant cells
    [~, pDTE(i)] = kstest2(DTE, DTE_mod(:));
    [~, pDICM(i)] = kstest2(DICM, DICM_mod(:));
    
    % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted fraction of cells that ended up in the ICM
    [~, pICM_size(i)] = kstest2(ICM_size, ICM_size_mod(:));
    
    % p-value from the two-sample Kolmogorov-Smirnov test between observed and predicted distributions of GFP+ cells in whole embryo
    [~, ppos(i)] = kstest2(pos, pos_mod(:));
    end
toc

hmp = 4 ./ (1./pDTE + 1./pDICM + 1./pICM_size + 1./ppos); % harmonic mean p-value

% fit a curve to the data
poly = polyfit(bias, log(hmp), 4);
hmpfit = exp(polyval(poly, bias));

%% plot of the harmonic mean p-value vs. the bias
figure('Position', [0 0 550 400])
hold on
box on
plot(100*bias, hmp, 'bo', 'LineWidth', 1, 'MarkerFaceColor', 'b')
xticks(10*floor(min(10*bias)):10:10*ceil(max(10*bias)))
xlim(minmax(xticks))
xticklabels(arrayfun(@(x) [num2str(x) ':' num2str(100-x)], xticks(), 'UniformOutput', false))
xlabel('Bias in fate determination (%)')
ylabel('Harmonic mean p-value')
set(gca, 'FontSize', FontSize, 'LineWidth', 1, 'YScale', 'log')
grid on
plot(100*bias, hmpfit, 'k-', 'Linewidth', 2)
yline(0.05, 'k--', 'LineWidth', 1)

name = 'p_value_bias';
savefig([name '.fig'])
saveas(gcf, [name '.png'])

T = table();
T.bias_pct = 100*bias';
T.hmp_value = hmp;
T.hmp_value_fit = hmpfit';
writetable(T, [name '.csv'])

end